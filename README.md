# Arm GNU Toolchain for Morello

Arm GNU Toolchain for Morello is a GNU toolchain with support for Morello, for AArch64 targets.

Arm GNU Toolchain for Morello is currently available for GNU/Linux host operating system on x86_64 and AArch64 architectures. Currently, it only supports aarch64-none-elf bare-metal and aarch64-none-linux-gnu targets.

For more information, refer to [Arm Morello project][arm_morello]

[arm_morello]: https://developer.arm.com/downloads/-/arm-gnu-toolchain-for-morello-downloads

